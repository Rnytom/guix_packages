(define-module (gnu packages firejail)
  #:use-module (gnu packages)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix licenses)
  #:use-module (guix build-system gnu))

(define-public firejail
  (package
   (name "firejail")
   (version "0.9.52")
   (source (origin
            (method url-fetch)
            (uri (string-append
                  "https://sourceforge.net/projects/firejail/files/firejail/firejail-"
                  version ".tar.xz/download"))
            (sha256
             (base32
              "0w8l8z4j7iph8fp7rchhnfsrik3f00f9v5xr191fp38fphzcj56s"))))
   (build-system gnu-build-system)

   (arguments
    '(#:make-flags (list (string-append "PREFIX=" (assoc-ref %outputs "out")))
      #:phases
      (modify-phases %standard-phases
                     (add-after
                      'configure 'allow-root-symlinks
                      (lambda _
                        ;; Allow whitelisting ~/.guix-profile
                        (substitute* "etc/firejail.config"
                                     (("# follow-symlink-as-user yes")
                                      "follow-symlink-as-user no"))))
                     (delete 'check))))
   (synopsis "Linux namespaces and seccomp-bpf sandbox")
   (description "Firejail is a SUID program that reduces the risk of
security breaches by restricting the running environment of untrusted
applications using Linux namespaces and seccomp-bpf.  It allows a
process and all its descendants to have their own private view of the
globally shared kernel resources, such as the network stack, process
table, mount table.")
   (home-page "https://firejail.wordpress.com")
   (license gpl2)))
