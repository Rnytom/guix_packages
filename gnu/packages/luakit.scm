(define-module (gnu packages luakit)
  #:use-module (gnu packages)
  #:use-module (gnu packages databases)
  #:use-module (gnu packages webkit)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages lua)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages gnome)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix licenses)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system glib-or-gtk))

(define-public luakit
  (package
    (name "luakit")
    (version "2017.08.10")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://github.com/luakit/luakit/archive/" version
			".tar.gz"))
              (sha256
               (base32
                "0dwxhnq90whakgdg21lzcf03n2g1c7hqgliwhav8av5na5mqpn93"))))
    (propagated-inputs
      `(	
	  ("gsettings-desktop-schemas", gsettings-desktop-schemas) ; needed or luakit gives an internal error
	  ("glib-networking", glib-networking) ; SSL support
	))
    (inputs
	`(("lua-5.1", lua-5.1)
	  ("gtk+" ,gtk+)
	  ("lua5.1-filesystem", lua5.1-filesystem)
	  ("luajit", luajit)
	  ("webkitgtk", webkitgtk)
	  ("sqlite", sqlite)
	  )
	)
    (native-inputs 
	`(
	("pkg-config", pkg-config)
	))
    
    
    (build-system glib-or-gtk-build-system)

    (arguments 
	'(#:make-flags
            (let ((out (assoc-ref %outputs "out"))) 
              (list 
                "CC=gcc" 
                "LUA_BIN_NAME=lua"
		"DEVELOPMENT_PATHS=0"
                (string-append "PREFIX=" out)
                (string-append "XDGPREFIX=" out "/etc/xdg")))
	 #:phases
       (modify-phases %standard-phases
		      (add-before 'build 'lfs-workaround
                    (lambda _
                      (setenv "LUA_CPATH"
                              (string-append (assoc-ref %build-inputs "lua5.1-filesystem") "/lib/lua/5.1/?.so;;"))
                      #t))
         (delete 'configure)
         (delete 'check)
         (add-after 'install 'wrap
           (lambda* (#:key inputs outputs #:allow-other-keys)
             (let* ((luakit (assoc-ref outputs "out"))
                    (lua5.1-filesystem (assoc-ref inputs "lua5.1-filesystem") )
		    (gtk (assoc-ref inputs "gtk+"))
                    (gtk-share (string-append gtk "/share")))
               (wrap-program (string-append luakit "/bin/luakit")
                 `("LUA_CPATH" prefix (,(string-append
                                          lua5.1-filesystem "/lib/lua/5.1/?.so;;")))
		 ))))
	 )))
    (synopsis "Luakit")
    (description "luakit is a fast, light and simple to use micro-browser framework extensible by Lua using the WebKit web content engine and the GTK+ toolkit.")
    (home-page "https://luakit.github.io/")
    (license gpl3)))
