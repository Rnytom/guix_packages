(define-module (gnu packages lua-cjson)
  #:use-module (gnu packages)
  #:use-module (gnu packages databases)
  #:use-module (gnu packages webkit)
  #:use-module (gnu packages lua)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages gnome)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix licenses)
  #:use-module (guix build-system gnu))

(define-public luakit
  (package
    (name "lua-cjson")
    (version "2.1.0")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://github.com/mpx/lua-cjson/archive/" version
			".tar.gz"))
              (sha256
               (base32
                "1kfqfxhbhrlanazfqvrh420zcyvm4k9ib8ys32ijgpvfsnbgcjc4"))))
    (inputs
	`(("lua-5.1", lua-5.1)))
  ;  (native-inputs 
;	`(
;	("pkg-config", pkg-config)
;	))
    
    
    (build-system gnu-build-system)

    (arguments 
	'(#:make-flags
            (let ((out (assoc-ref %outputs "out"))) 
              (list 
                "CC=gcc" 
                "LUA_BIN_NAME=lua"
		"DEVELOPMENT_PATHS=0"
                (string-append "PREFIX=" out)
                (string-append "XDGPREFIX=" out "/etc/xdg")))
	 #:phases
       (modify-phases %standard-phases
;		      (add-before 'build 'lfs-workaround
;                    (lambda _
;                      (setenv "LUA_CPATH"
;                              (string-append (assoc-ref %build-inputs "lua5.1-filesystem") "/lib/lua/5.1/?.so;;"))
;                      #t))
         (delete 'configure)
         (delete 'check)
;         (add-after 'install 'wrap
;           (lambda* (#:key inputs outputs #:allow-other-keys)
;             (let* ((luakit (assoc-ref outputs "out"))
;                    (lua5.1-filesystem (assoc-ref inputs "lua5.1-filesystem") ))
;               (wrap-program (string-append luakit "/bin/luakit")
;                 `("LUA_CPATH" suffix (,(string-append
;                                          lua5.1-filesystem "/lib/lua/5.1/?.so;;"))))))))))
)))
    (synopsis "lua-cjson")
    (description "Lua CJSON is a fast JSON encoding/parsing module for Lua ")
    (home-page "https://www.kyne.com.au/~mark/software/lua-cjson.php")
    (license fsf-free))) ; TODO: find the proper license
